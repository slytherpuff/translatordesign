ID [_a-zA-Z][_a-zA-Z0-9]*
DIGIT [0-9]
OP [+-/%<>=!&*]
INTEGER 0|[1-9]{DIGIT}*
FLOAT {DIGIT}*\.{DIGIT}+|{DIGIT}+\.{DIGIT}*
CHAR '([^'])?'
STRING \"([^"])*\"|\"\"
WHITESPACE [ \t]+
RVAL \{digit\}(?:(?!\.\{digit\}\+)(?:.|\n))*\.\{digit\}\+


/*** Definition Section has one variable 
which can be accessed inside yylex()  
and main() ***/
%{
  #include <stdio.h>
  #include <string.h>

  // Type of tokens.
  typedef enum {key, id, op,integer, flt, ch, str, nl, noth, error,lpar,rpar,lbrace,
		rbrace,lbrack,rbrack,comma,semicolon,colon,assign,or,
		not,and,add,sub,mul,div,equal,lt,gt,le,ge,bval} Type;
  typedef enum {false, true} bool; // Boolean algebra.

  void print(Type type); // Print out the output messages.

  int numLines = 0; // Count the number of lines of the input file.
  char line[1000] = ""; // Buffer for the texts of a line.
  bool inComment = false; // See if the current position is in a comment.
  bool mulComment = false; // See which type of comment.
  bool inError = false; // See if encounter error.
  bool sourceOn = true; // Control pragma source on or off.
  bool tokenOn = true; // Control pragma token on or off.
%}


/*** Rule Section has three rules, first rule  
matches with capital letters, second rule 
matches with any character except newline and  
third rule does not take input after the enter***/
%%

"#pragma source on" {sourceOn = true;
                     print(noth);}
"#pragma source off" {sourceOn = false;
                      print(noth);}
"#pragma token on" {tokenOn = true;
                    print(noth);}
"#pragma token off" {tokenOn = false;
                     print(noth);}
"#pragma". {print(error);}

"/*" {inComment = true;
      mulComment = true;
      print(noth);}
"%" {inComment = true;
      mulComment = false;
      print(noth);}
"*/" {inComment = false;
      mulComment = false;
      print(noth);}


"if" {print(key);}
"then" {print(key);}
"while" {print(key);}
"do" {print(key);}
"read" {print(key);}
"else" {print(key);}
"begin" {print(key);}
"end" {print(key);}
"print" {print(key);}
"int" {print(key);}
"bool" {print(key);}
"real" {print(key);}
"var" {print(key);}
"size" {print(key);}
"float" {print(key);}
"floor" {print(key);}
"ceil" {print(key);}
"fun" {print(key);}
"return" {print(key);}
"void" {print(key);}
"char" {print(key);}
"null" {print(key);}

"true" {print(bval);}
"false" {print(bval);}

"(" {print(lpar);}
")" {print(rpar);}
"{" {print(lbrace);}
"}" {print(rbrace);}
"[" {print(lbrack);}
"]" {print(rbrack);}

";" {print(semicolon);}
"," {print(comma);}
":" {print(colon);}
":=" {print(assign);}

"&&" {print(and);}
"||" {print(or);}
"not" {print(not);}
"+" {print(add);}
"-" {print(sub);}
"*" {print(mul);}
"/" {print(div);}
"equal" {print(equal);}
"<" {print(lt);}
">" {print(gt);}
"=<" {print(le);}
">=" {print(ge);}

{OP} {print(op);}

{FLOAT}{FLOAT}+ {print(error);}

{INTEGER} {print(integer);}

{FLOAT} {print(flt);}

{ID} {print(id);}

{CHAR} {print(ch);}

{STRING} {print(str);}

{DIGIT}[a-zA-Z0-9]+ {print(error);}
{DIGIT}+[eE][+-]?[a-zA-Z]+|{FLOAT}[eE][+-]?[a-zA-Z]+ {print(error);}
{ID}@+ {print(error);}

\n {inComment = (mulComment == true) ? true : false;
    mulComment = (inComment == 1) ? true : false;
    print(nl);
    strcpy(line, "");}

{WHITESPACE} {print(noth);}
'\" {print(noth);}
. {print(error);}

%%

/*** Code Section prints the number of 
capital letter present in the given input***/

int main(int argc, char **argv)
{
  yyin = fopen(argv[1], "r");
  yylex();
  return 0;
}

int yywrap()
{
  return 1; // EOF
}

void print(Type type)
{
  switch(type)
  {
    case key:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#key:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case id:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#id:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case op:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#op:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case punc:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#punc:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case integer:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#integer:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case flt:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#float:%s\n", yytext);
      }
      strcat(line, yytext);
      break;

    case ch:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#char:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case lpar:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#lpar:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case rpar:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#rpar:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case lbrace:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#lbrace:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case rbrace:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#rbrace:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case lbrack:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#lbrack:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
   case rbrack:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#rbrack:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case semicolon:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#semicolon:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case comma:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#comma:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case colon:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#colon:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case assign:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#assign:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case add:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#add:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case sub:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#sub:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case mul:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#assign:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case div:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#div:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case equal:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#equal:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case lt:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#lt:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case gt:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#gt:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case le:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#le:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case ge:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#ge:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case and:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#and:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case or:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#or:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case not:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#not:%s\n", yytext);
      }
      strcat(line, yytext);
      break;
    case bval:
      if (inComment == false && inError == false && tokenOn == true)
      {
        printf("#bval:%s\n", yytext);
      }
      strcat(line, yytext);
      break;



    case str:
      if (inComment == false && inError == false && tokenOn == true)
      {
        char printStr[1000];
        strncpy(printStr, yytext + 1, yyleng - 2);
        printStr[yyleng - 2] = '\0';
        printf("#string:%s\n", printStr);
      }
      strcat(line, yytext);
      break;

    case nl:
      if (inError == false && sourceOn == true)
      {
        printf("%d:%s\n", ++numLines, line);
        break;
      }
      else if (inError == false && sourceOn == false)
      {
        ++numLines;
        break;
      }
      else if (inError == true)
      {
        fprintf(stderr, "Error at line %d: %s\n", ++numLines, line);
        exit(1);
      }
      else
      {
        break;
      }

    case noth:
      strcat(line, yytext);
      break;

    case error:
      if (inComment == true)
      {
        break;
      }
      else
      {
        if (inError == false)
        {
          inError = true;
          strcpy(line, yytext);
        }
        else
        {
          strcat(line, yytext);
        }
      }
  }
}
